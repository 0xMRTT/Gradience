# Roadmap 

This tool is fully usable, if you want a new feature, open an issue or submit a PR

# Base features

- [x] Customize named colors, either with a color picker or with text
- [x] Explanations for some named colors
- [x] Partial theme preview
- [x] Built-in presets for Adwaita and Adwaita Dark (based on default libadwaita colors)
- [x] Apply changes to libadwaita, GTK4 (with extracted libadwaita theme) and GTK3 (with the adw-gtk3 theme) applications
- [x] Load and create custom presets
- [x] View adw-gtk3's support of variables
- [x] View parsing errors
- [x] Customize palette colors
- [x] Add custom CSS code
- [x] Localization support
- [x] Normalize color variables
- [x] Make the code more secure
- [x] Add preset manager with option to download other users presets
- [x] Release on Flathub
- [ ] Add a full theme preview instead of GTK4 Demo
- [x] Add autoload theme from CSS 
- [ ] Add ability to generate preset from one color

# Plugins

- [x] Add plugin support. Will help integration with others tools.
- [ ] Customize GNOME Shell **(High priority)**
- [ ] Customize GDM **(High priority)**
- [ ] Customize KvLibadwaita
- [x] Customize Firefox GNOME theme
